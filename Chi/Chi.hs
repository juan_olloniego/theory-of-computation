{-# OPTIONS_GHC -fno-warn-tabs #-}

module Chi where


data Exp = V Id | Ce Id | Lam [Id] Exp | Ap Exp [Exp] | Case Exp [Branch] | Rec Id Exp deriving Show

data FCD = Cc Id [Exp] | LamC [Id] Exp deriving Show

data Val = Cv Id [Val] | LamV [Id] Exp deriving Show 

type Id = String
type Branch = (Id, ([Id], Exp))
type Subst = (Id, Exp)


-- Deletes Tied Variables from a pending Substitution
(.-) :: [Subst] -> [Id] -> [Subst]
(.-) subs ids = filter (\(id, exp) -> not (elem id ids)) subs

-- Performs Multiple Substitution
(<==) :: Exp -> [Subst] -> Exp
(<==) (V id) subs = case lookup id subs of { Nothing -> V id; Just e -> e}
(<==) (Ce id) subs = Ce id
(<==) (Lam ids e) subs = Lam ids (e <== (subs .- ids))
(<==) (Ap e exps) subs = Ap (e <== subs) (map (<== subs) exps)
(<==) (Case e branches) subs = Case (e <== subs) (map (\(id, (ids, exp)) -> (id, (ids, exp <== (subs .- ids)))) branches)
(<==) (Rec id exp) subs = Rec id (exp <== (subs .- [id]))

-- Chi Weak Evaluation
weakEval :: Exp -> FCD
weakEval (V id) = error ("Variables have no evaluation, wrong variable was: " ++ id)
weakEval (Ce id) = Cc id []
weakEval (Lam ids e) = LamC ids e
weakEval (Ap e exps) = case weakEval e of {
							Cc id es -> Cc id (es ++ exps);
							LamC ids e -> case length ids == length exps of {
												False -> error "The length of the variables and arguments does not match."; 
												True -> weakEval (e <== (zip ids exps) )
							}
}
weakEval (Case e branches) = case weakEval e of {
									Cc id es -> case lookup id branches of {
													Nothing -> error ("The constructor was not found" ++ id);
													Just (ids, exp) -> case length ids == length es of {
																			False -> error "The length of the case variables and the assignable expressions does not match.";
																			True -> weakEval (exp <== (zip ids es))
													}
	                              };
	                              LamC ids b -> error "A lambda expression can't be used as a contructor in a branch."
}
weakEval (Rec id exp) = weakEval (exp <== [(id, Rec id exp)])

-- Performs Strong Evaluation
strongEval :: Exp -> Val
strongEval exp = case weakEval exp of {
	Cc id exps -> Cv id (map strongEval exps);
	LamC id e -> LamV id e
}


-- Funcions Coded In CHI

notChi :: Exp
notChi = Lam ["x"] (Case (V "x") [("False", ([], Ce "True")),
								  ("True", ([], Ce "False")) ])


evenChi :: Exp
evenChi = Rec "even" (Lam ["n"] (Case (V "n") [("O", ([], Ce "True")),
											   ("S", (["x"], Ap notChi [Ap (V "even") [V "x"]] ))]))


addChi :: Exp
addChi = Rec "add" (Lam ["n", "m"] (Case (V "n") [("O", ([], V "m")),
												("S", (["x"], Ap (Ce "S") [Ap (V "add") [(V "x"), (V "m")]]))]))


lengthChi :: Exp
lengthChi = Rec "length" (Lam ["lst"] (Case (V "lst") [("Nil", ([], Ce "O")),
													   ("Cons", (["x", "xs"], Ap (Ce "S") [Ap (V "length") [V "xs"]]))]))


mapChi :: Exp
mapChi = Rec "map" (Lam ["f", "lst"] (Case (V "lst") 
									[("Nil", ([], Ce "Nil")),
									 ("Cons", (["x", "xs"], (Ap (Ce "Cons") [(Ap (V "f") [V "x"]), (Ap (V "map") [(V "f"),(V "xs")])])))]))




-- Specifies how to parse a given data type to Chi Notation
class Chiable a where
	toChi :: a -> Exp

-- Parse Lists of ANY type to Chi Notation by making each type specify its Chi representation
instance (Chiable a) => Chiable [a] where
	toChi [] = Ce "Nil" 
	toChi (x:xs) = Ap (Ce "Cons") [toChi x, toChi xs]

-- We use Int to reference Natural numbers only. Loops for negatives.
instance Chiable Int where
	toChi 0 = Ce "O"
	toChi n = Ap (Ce "S") [toChi (n-1)]

instance Chiable Bool where
	toChi b = Ce (show b)


-- Functions to easily test the above

testEvenChi :: Int -> Val
testEvenChi n = do 
	let exp = (Ap evenChi [toChi n])
	let wellFormed = verifyWellFormed exp
	if wellFormed then strongEval exp else error "Malformed Expression"

testAddChi :: Int -> Int -> Val
testAddChi n m = strongEval (Ap addChi [toChi n, toChi m])

testLengthChi :: [Int] -> Val
testLengthChi lst = strongEval (Ap lengthChi [toChi lst])

testMapEvenChi :: [Int] -> Val
testMapEvenChi lst = strongEval (Ap mapChi [evenChi, toChi lst])


verifyWellFormed :: Exp -> Bool
verifyWellFormed (V id) = True
verifyWellFormed (Ce id) = True
verifyWellFormed (Lam ids exp) = verifyNonRepetedList ids && verifyWellFormed exp
verifyWellFormed (Ap e exps) = verifyWellFormed e && (foldr (&&) True (map verifyWellFormed exps))
verifyWellFormed (Case e branches) = verifyWellFormed e && (foldr (&&) True (map (\(id,(ids, exp)) -> verifyWellFormed exp) branches))
verifyWellFormed (Rec id exp) = verifyWellFormed exp

verifyNonRepetedList :: Eq a => [a] -> Bool
verifyNonRepetedList [] = True
verifyNonRepetedList (x:xs) = not (elem x xs) && verifyNonRepetedList xs