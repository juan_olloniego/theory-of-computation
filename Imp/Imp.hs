{-# OPTIONS_GHC -fno-warn-tabs #-}

module Imp where

type Id = String

type Memory = [(Id, Value)]

type Program = [Instruction]

type Branch = (Id, ([Id], Program))

data Expression = V Id | Ce Id [Expression] deriving Show

data Value = Cv Id [Value] deriving Show

data Instruction = Assign [(Id, Expression)] | Case Id [Branch] | While Id [Branch] deriving Show

-- Update Memory
(<<<) :: Memory -> Memory -> Memory
(<<<) original toAdd = toAdd ++ original

-- Wrap over lookup to avoid case every time
(>>>) :: Memory -> Id -> Value
(>>>) mem id = case lookup id mem of {Nothing -> error ("Variable: "++id++" was not found."); Just val -> val}

-- Evaluate an Expression on a given Memory
(//) :: Memory -> Expression -> Value
(//) mem (V id) = mem >>> id
(//) mem (Ce id exps) = Cv id (map (mem //) exps)

-- Execution of a step in the program
(|>) :: (Memory, Program) -> (Memory, Program)
(|>) (mem, ((Assign assigns):ps)) = case unzip assigns of {(ids, exps) -> (mem <<< (zip ids (map (mem //) exps)), ps)} 
(|>) (mem, ((Case id brs):ps)) = case (mem >>> id) of { Cv constuctor vals -> case lookup constuctor brs of {
																				Nothing -> error ("Could not find constuctor "++constuctor++" in the branches of the case.");
																				Just (ids, prog) -> (mem <<< (zip ids vals), prog++ps)}}

(|>) (mem, ((While id brs):ps)) = case (mem >>> id) of {Cv constuctor vals -> case lookup constuctor brs of {
																				Nothing -> (mem, ps);
																				Just (ids, prog) -> (mem <<< (zip ids vals), prog ++((While id brs):ps))}}

-- Run a full Program on a Memory
run :: (Memory, Program) -> Memory
run (mem, []) = mem
run (mem, (x:xs)) = run ((|>) (mem, (x:xs)))

-- Run a program and print its result (Stored in "result" variable in memory)
programResult :: Memory -> Program -> Value
programResult mem prog = run (mem, prog) >>> "result"


-- Imp Functions to test this.
initialNotMemory :: Memory
initialNotMemory = [("x", Cv "False" [])]

notImp :: Program
notImp = [Case "x" [
				("True", ([], [Assign [("result", Ce "False" [])]])),
				("False", ([], [Assign [("result", Ce "True" [])]]))
				]
		  ]

